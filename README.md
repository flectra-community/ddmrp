# Flectra Community / ddmrp

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[ddmrp_coverage_days](ddmrp_coverage_days/) | 2.0.1.1.0| Implements Coverage Days.
[ddmrp_chatter](ddmrp_chatter/) | 2.0.1.0.0| Adds chatter and activities to stock buffers.
[ddmrp_exclude_moves_adu_calc](ddmrp_exclude_moves_adu_calc/) | 2.0.1.0.0| Define additional rules to exclude certain moves from ADU calculation
[ddmrp_history](ddmrp_history/) | 2.0.1.0.0| Allow to store historical data of DDMRP buffers.
[ddmrp_packaging](ddmrp_packaging/) | 2.0.1.1.0| DDMRP integration with packaging
[ddmrp](ddmrp/) | 2.0.1.13.1| Demand Driven Material Requirements Planning
[ddmrp_warning](ddmrp_warning/) | 2.0.1.0.0| Adds configuration warnings on stock buffers.
[stock_buffer_route](stock_buffer_route/) | 2.0.1.1.0| Allows to force a route to be used when procuring from Stock Buffers
[ddmrp_cron_actions_as_job](ddmrp_cron_actions_as_job/) | 2.0.1.0.0| Run DDMRP Buffer Calculation as jobs
[stock_buffer_sales_analysis](stock_buffer_sales_analysis/) | 2.0.1.0.0| Allows to access the Sales Analysis from Stock Buffers
[ddmrp_adjustment](ddmrp_adjustment/) | 2.0.1.0.0| Allow to apply factor adjustments to buffers.
[ddmrp_purchase_hide_onhand_status](ddmrp_purchase_hide_onhand_status/) | 2.0.1.0.0| Replace purchase onhand status with smart button.
[stock_buffer_capacity_limit](stock_buffer_capacity_limit/) | 2.0.1.0.0| Ensures that the limits of storage are never surpassed
[ddmrp_product_replace](ddmrp_product_replace/) | 2.0.1.0.2| Provides a assisting tool for product replacement.


