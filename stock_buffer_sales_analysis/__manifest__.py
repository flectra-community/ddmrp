# Copyright 2021 Camptocamp SA
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl)
{
    "name": "Stock Buffer Sales Analysis",
    "summary": "Allows to access the Sales Analysis from Stock Buffers",
    "version": "2.0.1.0.0",
    "license": "LGPL-3",
    "website": "https://gitlab.com/flectra-community/ddmrp",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "category": "Warehouse",
    "depends": ["ddmrp", "sale", "sales_team"],
    "data": ["views/stock_buffer_views.xml"],
    "installable": True,
}
